#!/bin/bash
git clone git://git.buildroot.net/buildroot
cp mediaplayer.patch buildroot
cd buildroot
patch -p1 <mediaplayer.patch
mkdir -p board/raspberrypi/rootfs_overlay/etc
cp ../mdev.conf board/raspberrypi/rootfs_overlay/etc
cp ../automount board/raspberrypi/rootfs_overlay/etc
make raspberrypi2_media_defconfig
make


cd ..
cp buildroot/output/images/sdcard.img ./

